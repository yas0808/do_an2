@extends('admin.layout.index')
@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Sửa Sách
                            <small>{{$sach -> ten_sach}}</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif
                    <div class="col-lg-7" style="padding-bottom:120px">
                        @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all()
                            as $err)
                            {{$err}}<br>
                            @endforeach
                        </div>
                        @endif

                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif
                        <form action="tong/sach/sua_sach/{{$sach->ma_sach}}" method="POST"/>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                           <div class="form-group">
                            <label>Thể Loại Sách</label>
                            <select class="form-control" name="ma_the_loai_sach">
                            @foreach ($the_loai_sach as $tl)
                                @if($sach->ma_the_loai_sach == $tl->ma_sach)
                                {{"selected"}}
                                @endif
                                <option value="{{ $tl->ma_the_loai_sach }}">
                                    {{$tl->ten_the_loai_sach}}
                                </option>
                            @endforeach    
                            </select>
                        </div>

                        <div class="form-group">

                            <label>Tác Giả</label>
                            <select class="form-control" name="ma_tac_gia">
                              @foreach ($tac_gia as $tg)
                                @if($sach->ma_tac_gia == $tg->ma_sach)
                                {{"selected"}}
                                @endif
                                <option value="{{ $tg->ma_tac_gia }}">
                                    {{$tg->ten_tac_gia}}
                                </option>
                            @endforeach    
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <label>Nhà Xuất Bản</label>
                            <select class="form-control" name="ma_nha_xuat_ban">
                              @foreach ($nha_xuat_ban as $nxb)
                                @if($sach->ma_nha_xuat_ban == $nxb->ma_sach)
                                    {{"selected"}}
                                @endif
                                <option value="{{ $nxb->ma_nha_xuat_ban }}">
                                    {{$nxb->ten_nha_xuat_ban}}
                                </option>
                            @endforeach    
                            </select>
                        </div>
                            <div class="form-group">
                                <label>Tên Sách</label>
                                <input class="form-control" name="ten_sach" placeholder="Nhập tên sách" value="{{$sach->ten_sach}}" />
                            </div>
                            <div class="form-group">
                                <label>Giới Thiệu</label>
                                <input class="form-control" name="gioi_thieu" placeholder="Nhập giới thiệu" value="{{$sach->gioi_thieu}}" />
                            </div>
                             <div class="form-group">
                                <label>Số Lượng</label>
                                <input type="number" class="form-control " name="so_luong" placeholder="Nhập số lượng"
                                 value="{{$sach->so_luong}}"/>
                            </div>
                             <div class="form-group">
                                <label>Ngày Nhập</label>
                                <input type = "date"class="form-control" name="ngay_nhap" placeholder="Nhập ngày lưu kho" value="{{$sach->ngay_nhap}}"/>
                            </div>
                             <div class="form-group">
                                <label>Giá Tiền</label>
                                <input type="number" class="form-control" name="gia_tien" placeholder="Nhập số tiền"
                                value="{{$sach->gia_tien}}" />
                            </div>
                           
                            
                            <button type="submit" class="btn btn-default">Sửa</button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection



