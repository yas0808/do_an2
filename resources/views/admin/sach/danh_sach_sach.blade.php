@extends('admin.layout.index')
@section('content')
 <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Sách 
                            <small>Kho Sách</small>
                        </h1>
                    </div>
                    <br>
                    <!-- /.col-lg-12 -->
                    <div>
                        <p>
                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif
                    </p>
                    </div>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>STT</th>
                                
                                <th>Tác Giả</th>
                                <th>Thể loại sách</th>
                                <th>Nhà Xuất Bản</th>
                                <th>Tên Sách</th>
                                <th>Giới Thiệu</th>
                                <th>Số Lượng</th>
                                <th>Ngày Nhập</th>
                                <th>Giá Tiền</th>
                                
                                <th>Sửa</th>
                                <th>Xóa</th>
                            </tr>
                        </thead>
                        <tbody>
                           @foreach($sach as $sh)
                            <tr class="even gradeC" align="center">
                                @foreach($arrays as $STT)
                                 {{ $STT->ten_tac_gia }} - {{ $loop->index }} of {{ $loop->count }}
                                @endforeach
                                
                                <td>{{$sh->tac_gia->ten_tac_gia}}</td>
                                <td>{{$sh->the_loai_sach->ten_the_loai_sach}}</td>
                                <td>{{$sh->nha_xuat_ban['ten_nha_xuat_ban']}}</td>
                                <td>{{$sh->ten_sach}}</td>
                                <td>{{$sh->gioi_thieu}}</td>
                                <td>{{$sh->so_luong}}</td>
                                <td>{{$sh->ngay_nhap}}</td>
                                <td>
                                    <?php echo number_format($sh->gia_tien); ?>
                                </td>
                                
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="tong/sach/sua_sach/{{$sh->ma_sach}}"> Edit</a></td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="tong/sach/xoa_sach/{{$sh->ma_sach}}">Delete</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection