@extends('doc_gia.layout.index')
@section('content')
 <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Đặt Mượn
                            <small>Danh Sách Đặt Mượn</small>
                        </h1>
                    </div>
                    <br>
                    <!-- /.col-lg-12 -->
                    <div>
                        <p>
                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif
                    </p>
                    </div>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>Mã Đặt Mượn</th>
                                <th>Tên Sách</th>
                                <th>Tên Độc Giả</th>
                                <th>Ngày Đặt</th>
                                <th>Trạng Thái</th>
                                <th>Số Lượng</th>
                               
                            </tr>
                        </thead>
                        <tbody>
                           @foreach($dat_muon as $dm)
                            <tr class="even gradeC" align="center">
                                <td>{{$dm->ma_dat_muon}}</td>
                                <td>{{$dm->sach->ten_sach}}</td>
                                <td>{{$dm->doc_gia->ten_doc_gia}}</td>
                                <td>{{$dm->ngay_dat}}</td>
                                <td>@if($dm->trang_thai == 1)
                                    {{"Đã Trả"}}
                                    @else
                                    {{"Chưa Trả"}}
                                    @endif</td>
                                <td>{{$dm->so_luong}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection