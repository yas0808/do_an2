@extends('admin.layout.index')
@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Đặt Mượn
                            <small>Thêm Đặt Mượn</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all()
                            as $err)
                            {{$err}}<br>
                            @endforeach
                        </div>
                        @endif

                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif
                        <form action="tong/dat_muon/them_dat_muon" method="POST"/>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="form-group">
                                <label>Tên Độc Giả</label>
                               <select class="form-control" name="doc_gia">
                              @foreach ($doc_gia as $dg)
                                <option value="{{ $dg->ma_doc_gia }}">
                                    {{$dg->ten_doc_gia}}
                                </option>
                            @endforeach    
                            </select>
                            </div>
                            <div class="form-group">
                                <label>Tên Sách</label>
                               <select class="form-control" name="sach">
                              @foreach ($sach as $sh)
                                <option value="{{ $sh->ma_sach }}">
                                    {{$sh->ten_sach}}
                                </option>
                            @endforeach    
                            </select>
                            </div>
                            <div class="form-group">
                                <label>Ngày Đặt Mượn</label>
                                <input class="form-control" type = "date" name="ngay_dat" placeholder="Nhập ngày đặt mượn" />
                            </div>
                            <div class="form-group">
                                <label>Số Lượng</label>
                                <input class="form-control" type = "number" name="so_luong" placeholder="Nhập số Lượng" />
                            </div>
                            <div class="form-group">
                                <label>Trạng Thái</label>
                                <label class="radio-inline">
                                    <input name="trang_thai" value="0" checked="" type="radio">Đã Trả
                                </label>
                                <label class="radio-inline">
                                    <input name="cap_do" value="1" type="radio">Chưa Trả
                                </label>
                            </div>
                            <button type="submit" class="btn btn-default">Thêm</button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection