@extends('admin.layout.index')
@section('content')
 <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Admin
                            <small>Danh sách admin</small>
                        </h1>
                    </div>
                    <br>
                    <!-- /.col-lg-12 -->
                    <div>
                        <p>
                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif
                    </p>
                    </div>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>Mã admin</th>
                                <th>Tên admin</th>
                                <th>Email</th>
                                <th>Mật khẩu</th>
                                <th>Địa chỉ</th>
                                <th>Ngày sinh</th>
                                <th>Giới tính</th>
                                <th>SĐT</th>
                                <th>Cấp độ</th>
                                
                                <th>Sửa</th>
                                <th>Xóa</th>
                            </tr>
                        </thead>
                        <tbody>
                           @foreach($admin as $ad)
                            <tr class="even gradeC" align="center">
                                <td>{{$ad->ma_admin}}</td>
                                <td>{{$ad->ten_admin}}</td>
                                <td>{{$ad->email}}</td>
                                <td>{{$ad->password}}</td>
                                <td>{{$ad->dia_chi}}</td>
                                <td>{{$ad->ngay_sinh}}</td>
                                <td>@if($ad->gioi_tinh == 1)
                                    {{"Nữ"}}
                                    @else
                                    {{"Nam"}}
                                    @endif</td>
                                <td>{{$ad->sdt}}</td>
                                <td>
                                    @if($ad->cap_do == 1)
                                    {{"Admin"}}
                                    @else
                                    {{"SuperAdmin"}}
                                    @endif
                                </td>
                                
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="tong/admin/sua_admin/{{$ad->ma_admin}}"> Edit</a></td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="tong/admin/xoa_admin/{{$ad->ma_admin}}">Delete</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection