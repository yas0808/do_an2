<div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <!-- <li>
                            <a href="#"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li> -->
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Admin<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="tong/admin/danh_sach_admin">Danh Sách Admin</a>
                                </li>
                                <li>
                                    <a href="tong/admin/them_admin">Thêm Admin</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        

                        <li>
                            <a href="#"><i class="fa fa-cube fa-fw"></i> Sách<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="tong/sach/danh_sach_sach">Kho sách</a>
                                </li>
                                <li>
                                    <a href="tong/sach/them_sach">Thêm Sách</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-users fa-fw"></i> Đặt Mượn<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="tong/dat_muon/danh_sach_dat_muon">Danh Sách Đặt Mượn</a>
                                </li>
                                <li>
                                    <a href="tong/dat_muon/them_dat_muon">Thêm Đặt Mượn</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-cube fa-fw"></i> Độc Giả<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="tong/doc_gia/danh_sach_doc_gia">Danh Sách Độc Giả</a>
                                </li>
                                <li>
                                    <a href="tong/doc_gia/them_doc_gia">Thêm Độc Giả</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        
                        <li>
                            <a href="#"><i class="fa fa-cube fa-fw"></i> Ngành<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="tong/nganh/danh_sach_nganh">Danh Sách Ngành</a>
                                </li>
                                <li>
                                    <a href="tong/nganh/them_nganh">Thêm Sách Ngành</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-cube fa-fw"></i> Nhà Xuất Bản<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="tong/nha_xuat_ban/danh_sach_nha_xuat_ban">Danh Sách Nhà Sản Xuất</a>
                                </li>
                                <li>
                                    <a href="tong/nha_xuat_ban/them_nha_xuat_ban">Thêm Nhà Sản Xuất</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        
                        <li>
                            <a href="#"><i class="fa fa-cube fa-fw"></i> Tác Giả<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="tong/tac_gia/danh_sach_tac_gia">Danh Sách Tác Giả</a>
                                </li>
                                <li>
                                    <a href="tong/tac_gia/them_tac_gia">Thêm  Tác Giả</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        
                        <li>
                            <a href="#"><i class="fa fa-cube fa-fw"></i> Thể Loại Sách<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="tong/the_loai_sach/danh_sach_the_loai_sach">Danh Sách Thể Loại Sách</a>
                                </li>
                                <li>
                                    <a href="tong/the_loai_sach/them_the_loai_sach">Thêm Sách Thể Loại Sách</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                         <li>
                            <a href="#"><a href="{{url('logout')}}"><i class="fa fa-cube fa-fw"></i>Đăng Xuất</a>
                            
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->