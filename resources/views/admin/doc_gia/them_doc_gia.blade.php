@extends('admin.layout.index')
@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Độc Giả
                            <small>Thêm độc giả</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all()
                            as $err)
                            {{$err}}<br>
                            @endforeach
                        </div>
                        @endif

                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif
                        <form action="tong/doc_gia/them_doc_gia" method="POST"/>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">

                        <div class="form-group">
                            <label>Ngành</label>
                            <select class="form-control" name="nganh">
                              @foreach ($nganh as $ng)
                                <option value="{{ $ng->ma_nganh }}">
                                    {{$ng->ten_nganh}}
                                </option>
                            @endforeach    
                            </select>
                        </div>
                        <div class="form-group">
                                <label>Tên độc giả</label>
                                <input class="form-control" name="ten_doc_gia" placeholder="Nhập tên độc giả" />
                        </div>
                        <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" name="email" placeholder="Nhập email" />
                            </div>
                             <div class="form-group">
                                <label>Địa Chỉ</label>
                                <input class="form-control" name="dia_chi" placeholder="Nhập địa chỉ" />
                            </div>
                             <div class="form-group">
                                <label>Ngày Sinh</label>
                                <input type = "date"class="form-control" name="ngay_sinh" placeholder="Nhập ngày sinh" />
                            </div>
                             <div class="form-group">
                                <label>SĐT</label>
                                <input type="number" class="form-control" name="sdt" placeholder="Nhập số điện thoại" />
                            </div>
                           
                            <div class="form-group">
                                <label>Giới Tính</label>
                                <label class="radio-inline">
                                    <input name="gioi_tinh" value="0" checked="" type="radio">Nam
                                </label>
                                <label class="radio-inline">
                                    <input name="gioi_tinh" value="1" type="radio">Nữ
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Cấp Độ</label>
                                <label class="radio-inline">
                                    <input name="cap_do" value="0" checked="" type="radio">sinh viên 
                                </label>
                                <label class="radio-inline">
                                    <input name="cap_do" value="1" type="radio">Giảng Viên
                                </label>
                            </div>
                            <button type="submit" class="btn btn-default">Thêm</button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection