<div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <!-- <li>
                            <a href="#"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li> -->
                       
                        

                        <li>
                            <a href="tong/sach/danh_sach_sach_dg"><i class="fa fa-cube fa-fw"></i> Kho sách</a>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-users fa-fw"></i> Đặt Mượn<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="tong/dat_muon/danh_sach_dat_muon_dg">Danh Sách Đặt Mượn</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                       
                        <li>
                            <a href="#"><i class="fa fa-cube fa-fw"></i> Thông tin khác<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="tong/nha_xuat_ban/danh_sach_nha_xuat_ban_dg">Danh Sách Nhà Sản Xuất</a>
                                </li>
                               <li>
                                    <a href="tong/tac_gia/danh_sach_tac_gia_dg">Danh Sách Tác Giả</a>
                                </li>
                                <li>
                                    <a href="tong/the_loai_sach/danh_sach_the_loai_sach_dg">Danh Sách Thể Loại Sách</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                         <li>
                            <a href="#"><a href="{{url('logout')}}"><i class="fa fa-cube fa-fw"></i>Đăng Xuất</a>
                            
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->