@extends('admin.layout.index')
@section('content')
 <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Nhà Xuất Bản
                            <small>Danh sách nhà xuất bản</small>
                        </h1>
                    </div>
                    <br>
                    <!-- /.col-lg-12 -->
                    <div>
                        <p>
                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif
                    </p>
                    </div>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>Mã nhà xuất bản</th>
                                <th>Tên nhà xuất bản</th>                             
                                <th>Sửa</th>
                                <th>Xóa</th>
                            </tr>
                        </thead>
                        <tbody>
                           @foreach($nha_xuat_ban as $xb)
                            <tr class="even gradeC" align="center">
                                <td>{{$xb->ma_nha_xuat_ban}}</td>
                                <td>{{$xb->ten_nha_xuat_ban}}</td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="tong/nha_xuat_ban/sua_nha_xuat_ban/{{$xb->ma_nha_xuat_ban}}"> Edit</a></td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="tong/nha_xuat_ban/xoa_nha_xuat_ban/{{$xb->ma_nha_xuat_ban}}">Delete</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection