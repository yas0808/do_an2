@extends('doc_gia.layout.index')
@section('content')
 <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Nhà Xuất Bản
                            <small>Danh sách nhà xuất bản</small>
                        </h1>
                    </div>
                    <br>
                    <!-- /.col-lg-12 -->
                    <div>
                        <p>
                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif
                    </p>
                    </div>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                               <th>STT</th>
                                <th>Tên nhà xuất bản</th>                             
                                
                            </tr>
                        </thead>
                        <tbody>
                           @foreach($nha_xuat_ban as $xb)
                            <tr class="even gradeC" align="center">
                                @foreach($arrays as $STT)
                                 {{ $STT->ten_nha_xuat_ban }} - {{ $loop->index }} of {{ $loop->count }}
                                @endforeach
                                <td>{{$xb->ten_nha_xuat_ban}}</td>
        
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection