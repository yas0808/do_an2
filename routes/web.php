<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('admin/dang_nhap_admin', 'Admin_Controller@Dang_nhap_admin');
Route::post('admin/dang_nhap_admin', 'Admin_Controller@postDang_nhap_admin');
Route::get('logout','Admin_Controller@getLogout');
Route::get('doc_gia/dang_nhap_doc_gia', 'Doc_gia_Controller@Dang_nhap_doc_gia');
Route::get('doc_gia/dang_nhap_doc_gia', 'Doc_gia_Controller@postDang_nhap_doc_gia');

Route::group(['prefix'=>'tong'], function(){
	Route::group(['prefix'=>'dat_muon'], function(){
		Route::get('danh_sach_dat_muon','Dat_muon_Controller@Danh_sach_dat_muon');
		Route::get('danh_sach_dat_muon/{id}','Dat_muon_Controller@Danh_sach_dat_muon_dg');

		Route::get('sua_dat_muon/{id}','Dat_muon_Controller@Sua_dat_muon');
		Route::post('sua_dat_muon/{id}','Dat_muon_Controller@postSua_dat_muon');

		Route::get('them_dat_muon','Dat_muon_Controller@Them_dat_muon');
		Route::post('them_dat_muon','Dat_muon_Controller@postThem_dat_muon');

		Route::get('xoa_dat_muon/{id}','Dat_muon_Controller@Xoa_dat_muon');
	});

	Route::group(['prefix'=>'doc_gia'], function(){
		Route::get('danh_sach_doc_gia','Doc_gia_Controller@Danh_sach_doc_gia');

		Route::get('sua_doc_gia/{id}','Doc_gia_Controller@Sua_doc_gia');
		Route::post('sua_doc_gia/{id}','Doc_gia_Controller@postSua_doc_gia');

		Route::get('them_doc_gia/','Doc_gia_Controller@Them_doc_gia');
		Route::post('them_doc_gia/','Doc_gia_Controller@postThem_doc_gia');

		Route::get('xoa_doc_gia/{id}','Doc_gia_Controller@Xoa_doc_gia');
	});


	
	// Route::group(['prefix'=>'don_dat_muon'], function(){
	// 	Route::get('danh_sach_don_dat_muon','Don_dat_muon_Controller@Danh_sach_don_dat_muon');

	// 	Route::get('sua_don_dat_muon/{id}','Don_dat_muon_Controller@Sua_don_dat_muon');
	// 	Route::post('sua_don_dat_muon/{id}','Don_dat_muon_Controller@postSua_don_dat_muon');

	// 	Route::get('them_don_dat_muon','Don_dat_muon_Controller@Them_don_dat_muon');
	// 	Route::post('them_don_dat_muon','Don_dat_muon_Controller@postThem_don_dat_muon');

	// 	Route::get('xoa_don_dat_muon/{id}','Don_dat_muon_Controller@Xoa_don_dat_muon');
	// });


	Route::group(['prefix'=>'nganh'], function(){
		Route::get('danh_sach_nganh','Nganh_Controller@Danh_sach_nganh');

		Route::get('sua_nganh/{id}','Nganh_Controller@Sua_nganh');
		Route::post('sua_nganh/{id}','Nganh_Controller@postSua_nganh');

		Route::get('them_nganh','Nganh_Controller@Them_nganh');
		Route::post('them_nganh','Nganh_Controller@postThem_nganh');

		Route::get('xoa_nganh/{id}','Nganh_Controller@Xoa_nganh');
	});




	Route::group(['prefix'=>'nha_xuat_ban'], function(){
		Route::get('danh_sach_nha_xuat_ban','Nha_xuat_ban_Controller@Danh_sach_nha_xuat_ban');
		Route::get('danh_sach_nha_xuat_ban','Nha_xuat_ban_Controller@Danh_sach_nha_xuat_ban_dg');

		Route::get('sua_nha_xuat_ban/{id}','Nha_xuat_ban_Controller@Sua_nha_xuat_ban');
		Route::post('sua_nha_xuat_ban/{id}','Nha_xuat_ban_Controller@postSua_nha_xuat_ban');

		Route::get('them_nha_xuat_ban','Nha_xuat_ban_Controller@Them_nha_xuat_ban');
		Route::post('them_nha_xuat_ban','Nha_xuat_ban_Controller@postThem_nha_xuat_ban');

		Route::get('xoa_nha_xuat_ban/{id}','Nha_xuat_ban_Controller@Xoa_nha_xuat_ban');
	});





	Route::group(['prefix'=>'sach'], function(){
		Route::get('danh_sach_sach','Sach_Controller@Danh_sach_sach');
		Route::get('danh_sach_sach','Sach_Controller@Danh_sach_sach_dg');

		Route::get('sua_sach/{id}','Sach_Controller@Sua_sach');
		Route::post('sua_sach/{id}','Sach_Controller@postSua_sach');

		Route::get('them_sach','Sach_Controller@Them_sach');
		Route::post('them_sach','Sach_Controller@postThem_sach');

		Route::get('xoa_sach/{id}','Sach_Controller@Xoa_sach');
	});



	Route::group(['prefix'=>'tac_gia'], function(){
		Route::get('danh_sach_tac_gia','Tac_gia_Controller@Danh_sach_tac_gia');
		Route::get('danh_sach_tac_gia','Tac_gia_Controller@Danh_sach_tac_gia_dg');
		
		Route::get('sua_tac_gia/{id}','Tac_gia_Controller@Sua_tac_gia');
		Route::post('sua_tac_gia/{id}','Tac_gia_Controller@postSua_tac_gia');

		Route::get('them_tac_gia','Tac_gia_Controller@Them_tac_gia');
		Route::post('them_tac_gia','Tac_gia_Controller@postThem_tac_gia');

		Route::get('xoa_tac_gia/{id}','Tac_gia_Controller@Xoa_tac_gia');

	});






	Route::group(['prefix'=>'the_loai_sach'], function(){
		Route::get('danh_sach_the_loai_sach','The_loai_sach_Controller@Danh_sach_the_loai_sach');
		Route::get('danh_sach_the_loai_sach','The_loai_sach_Controller@Danh_sach_the_loai_sach_dg');

		Route::get('sua_the_loai_sach/{id}','The_loai_sach_Controller@Sua_the_loai_sach');
		Route::post('sua_the_loai_sach/{id}','The_loai_sach_Controller@postSua_the_loai_sach');

		Route::get('them_the_loai_sach','The_loai_sach_Controller@Them_the_loai_sach');
		Route::post('them_the_loai_sach','The_loai_sach_Controller@postThem_the_loai_sach');

		Route::get('xoa_the_loai_sach/{id}','The_loai_sach_Controller@Xoa_the_loai_sach');
	});





	Route::group(['prefix'=>'admin'], function(){
		Route::get('danh_sach_admin','Admin_Controller@Danh_sach_admin');
		
		Route::get('sua_admin/{id}','Admin_Controller@Sua_admin');
		Route::post('sua_admin/{id}','Admin_Controller@postSua_admin');

		Route::get('them_admin','Admin_Controller@Them_admin');
		Route::post('them_admin','Admin_Controller@postThem_admin');

		Route::get('xoa_admin/{id}','Admin_Controller@Xoa_admin');

	});
	Auth::routes();
	Route::get('/changePassword','HomeController@showChangePasswordForm');
Route::post('/changePassword','HomeController@changePassword')->name('changePassword');
});
