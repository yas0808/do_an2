<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class nganh extends Model
{
   protected $table = "nganh";
	protected $primaryKey = 'ma_nganh';
	public $timestamps = false;
   public function doc_gia()
   {
   	return $this->hasOne('App\doc_gia','ma_nganh','ma_doc_gia');
   }
}
