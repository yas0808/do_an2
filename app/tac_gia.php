<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tac_gia extends Model
{
   protected $table = "tac_gia";

   protected $primaryKey = 'ma_tac_gia';
   public $timestamps = false;

   public function sach()
   {
   	return $this->belongsTo('App\sach','ma_tac_gia','ma_sach');
   }
}
