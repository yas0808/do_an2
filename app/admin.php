<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;

class admin extends Model implements Authenticatable

{
	use AuthenticableTrait;
    protected $table="admin";

    protected $primaryKey = 'ma_admin';
}
