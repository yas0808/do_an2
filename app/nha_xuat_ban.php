<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class nha_xuat_ban extends Model
{
    protected $table = "nha_xuat_ban";
    protected $primaryKey = 'ma_nha_xuat_ban';
    public $timestamps = false;
    public function sach()
    {
    	return $this->hasMany('App\sach','ma_nha_xuat_ban','ma_sach');
    }
}
