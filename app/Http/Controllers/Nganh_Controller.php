<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\nganh;
class Nganh_Controller extends Controller

{
    public function Danh_sach_nganh()
	{
		$nganh= nganh::all();
		return view('admin/nganh/danh_sach_nganh', ['nganh'=> $nganh]);
	}
	public function Them_nganh()
	{
		return view('admin/nganh/them_nganh');
	}
	public function postThem_nganh(Request $request)
	{
		$this->validate($request, [
			'ten_nganh' =>'required|min:3',
			
			
		],
		[
			'ten_nganh.required' =>'Bạn chưa nhập tên ngành ',
			'ten_nganh.min' =>'Tên ngành sách phải có ít nhất 3 kí tự',
			
			
		]);
		$nganh = new nganh;
		$nganh->ten_nganh = $request->ten_nganh;
		
		$nganh->save();

		return redirect('tong/nganh/them_nganh')->with('thongbao','Thêm thành công');
	}
	public function Sua_nganh($ma_nganh)
	{
		$nganh = nganh::find($ma_nganh);
		return view('admin/nganh/sua_nganh',['nganh' => $nganh]);
	}
	public function postSua_nganh(Request $request,$ma_nganh)
	{
		$this->validate($request, [
			'ten_nganh' =>'required|min:3',
			
			
		],
		[
			'ten_nganh.required' =>'Bạn chưa nhập tên ngành ',
			'ten_nganh.min' =>'Tên ngành sách phải có ít nhất 3 kí tự',
			
			
		]);
		$nganh = nganh::find($ma_nganh);
		$nganh->ten_nganh = $request->ten_nganh;
		
		$nganh->save();


		return redirect('tong/nganh/sua_nganh/'.$ma_nganh)->with('thongbao','sửa thành công');
	}

	public function Xoa_nganh($id)
	{
		$nganh = nganh::find($id);
		$nganh->delete();
		return redirect('tong/nganh/danh_sach_nganh')->with('thongbao','xóa thành công');
	}
}

