<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\admin;

class Admin_Controller extends Controller
{
    public function Danh_sach_admin()
	{
		$admin= admin::all();
		return view('admin/use/danh_sach_admin', ['admin'=> $admin]);
	}
	
	public function Them_admin()
	{
		return view('admin/use/them_admin');
	}
	public function postThem_admin(Request $request)
	{
		$this->validate($request, [
			'ten_admin' =>'required|min:3',
			'email' =>'required|email|unique:admin,email',
			'password' =>'required|min:5|max:20',
			'passwordAgain' =>'required|same:password',
			'dia_chi' => 'required|min:5|max:100',
			'ngay_sinh' => 'required',
			'sdt' => 'required',
			'gioi_tinh' => 'required',
			'cap_do' => 'required',
		],
		[
			'ten_admin.required' =>'Bạn chưa nhập tên ',
			'ten_admin.min' =>'Tên người dùng phải có ít nhất 3 kí tự',

			'email.required' =>'Bạn chưa nhập Email',
			'email.email' =>'Bạn chưa nhập Đúng Email',
			'email.unique' =>'Email của bạn không tồn tại hoặc đã có tài khoản ',
			'password.required' =>'Bạn chưa nhập mật khẩu',
			'password.min' =>'Mật khẩu phải có ít nhất 5 kí tự',
			'password.max' =>'Mật khẩu phải có tối đa 20 kí tự',
			'passwordAgain.required' =>'Bạn chưa nhập lại mật khẩu',
			'passwordAgain.same' =>'Mật khẩu nhập lại chưa khớp',
			'dia_chi.min.required' =>'Bạn chưa nhập địa chỉ',
			'dia_chi.min' =>'Địa chỉ của bạn phải có ít nhất 3 kí tự',
			'ngay_sinh.required' =>'Bạn chưa nhập ngày sinh',
			'sdt.required' =>'Bạn chưa nhập số điện thoại ',	
			'gioi_tinh.required' =>'Bạn chưa nhập giới tính',
			'cap_do.required' =>'Bạn chưa nhập cấp độ',
			
		]);
		$admin = new admin;
		$admin->ten_admin = $request->ten_admin;
		$admin->email = $request->email;
		$admin->password = bcrypt($request->password);
		$admin->dia_chi = $request->dia_chi;
		$admin->ngay_sinh= $request->ngay_sinh;
		$admin->sdt = $request->sdt;
		$admin->gioi_tinh = $request->gioi_tinh;
		$admin->cap_do = $request->cap_do;
		$admin->save();

		return redirect('tong/admin/them_admin')->with('thongbao','Thêm thành công');
	}
	public function Sua_admin($ma_admin)
	{
		$admin = admin::find($ma_admin);
		return view('admin/use/sua_admin',['admin' => $admin]);
	}

	public function postSua_admin(Request $request,$ma_admin)
	{
		$this->validate($request, [
			'ten_admin' =>'required|min:3',
			'dia_chi' => 'required|min:5|max:100',
			'ngay_sinh' => 'required',
			'sdt' => 'required',
			'gioi_tinh' => 'required',
			
		],
		[
			'ten_admin.required' =>'Bạn chưa nhập tên ',
			'ten_admin.min' =>'Tên người dùng phải có ít nhất 3 kí tự',
			'dia_chi.min.required' =>'Bạn chưa nhập địa chỉ',
			'dia_chi.min' =>'Địa chỉ của bạn phải có ít nhất 3 kí tự',
			'ngay_sinh.required' =>'Bạn chưa nhập ngày sinh',
			'sdt.required' =>'Bạn chưa nhập số điện thoại ',	
			'gioi_tinh.required' =>'Bạn chưa nhập giới tính',
		]);
		$admin = admin::find($ma_admin);
		$admin->ten_admin = $request->ten_admin;
		$admin->dia_chi = $request->dia_chi;
		$admin->ngay_sinh= $request->ngay_sinh;
		$admin->sdt = $request->sdt;
		$admin->gioi_tinh = $request->gioi_tinh;
		$admin->save();

		return redirect('tong/admin/sua_admin/'.$ma_admin)->with('thongbao','sửa thành công');
	}
	public function Xoa_admin($id)
	{
		$admin = admin::find($id);
		$admin->delete();
		return redirect('tong/admin/danh_sach_admin')->with('thongbao','xóa thành công');
	}
	public function Dang_nhap_admin()
	{
		return view('admin/dang_nhap_admin');
	}
	public function postDang_nhap_admin(Request $request)
	{
		$this->validate($request, [
			
			'email' =>'required',
			'password' =>'required|min:5|max:20',
			
		],
		[

			'email.required' =>'Bạn chưa nhập Email',
			'password.required' =>'Bạn chưa nhập mật khẩu',
			'password.min' =>'Mật khẩu phải có ít nhất 5 kí tự',
			'password.max' =>'Mật khẩu phải có tối đa 20 kí tự',
			
		]);

		if(Auth::attempt(['email'=>$request->email,'password'=>$request->password]))
		{
			return redirect('tong/sach/danh_sach_sach');
			
		}	
		else
		{
			return redirect('admin/dang_nhap_admin')->with('thongbao','đăng nhập không thành công');
		}
	}
	public function getLogout() {
		Auth::logout();
		return redirect('admin/dang_nhap_admin');
	}

	
}
