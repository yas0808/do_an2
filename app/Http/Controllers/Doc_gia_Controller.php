<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\doc_gia;
use App\nganh;

class Doc_gia_Controller extends Controller
{
    public function Danh_sach_doc_gia()
	{
		$doc_gia = doc_gia::all();
		$nganh = nganh::all();
		// dd($doc_gia->toArray());
		return view('admin/doc_gia/danh_sach_doc_gia',[
			'doc_gia'=> $doc_gia,
			'nganh'=> $nganh 
		]);
		
	}
	public function Them_doc_gia()
	{	
		$doc_gia = doc_gia::all();
		$nganh = nganh::all();
		return view('admin/doc_gia/them_doc_gia',[
			'doc_gia'=> $doc_gia,
			'nganh'=> $nganh 
			
		]);
	}
	public function postThem_doc_gia(Request $request)

	{
		$this->validate($request, [
			'nganh'=>'required',
			'ten_doc_gia' =>'required|min:3',
			'email' =>'required|email|unique:doc_gia,email',
			'dia_chi' => 'required|min:5|max:100',
			'ngay_sinh' => 'required',
			'sdt' => 'required',
			'gioi_tinh' => 'required',
			'cap_do' => 'required',
			
		],
		[
			'nganh.required' =>'Bạn chưa chọn tên ngành ',
			'ten_doc_gia.required' =>'Bạn chưa nhập tên ',
			'ten_doc_gia.min' =>'Tên người dùng phải có ít nhất 3 kí tự',

			'email.required' =>'Bạn chưa nhập Email',
			'email.email' =>'Bạn chưa nhập Đúng Email',
			'email.unique' =>'Email của bạn không tồn tại hoặc đã có tài khoản ',
			'dia_chi.min.required' =>'Bạn chưa nhập địa chỉ',
			'dia_chi.min' =>'Địa chỉ của bạn phải có ít nhất 3 kí tự',
			'ngay_sinh.required' =>'Bạn chưa nhập ngày sinh',
			'sdt.required' =>'Bạn chưa nhập số điện thoại ',	
			'gioi_tinh.required' =>'Bạn chưa nhập giới tính',
			'cap_do.required' =>'Bạn chưa nhập cấp độ',
			
		]);
		$dg = new doc_gia;
		$dg->ma_nganh = $request->nganh;
		$dg->ten_doc_gia = $request->ten_doc_gia;
		$dg->email = $request->email;
		$dg->dia_chi = $request->dia_chi;
		$dg->ngay_sinh= $request->ngay_sinh;
		
		$dg->gioi_tinh = $request->gioi_tinh;
		$dg->cap_do = $request->cap_do;
		$dg->sdt = $request->sdt;
		$dg->save();

		return redirect('tong/doc_gia/them_doc_gia')->with('thongbao','Thêm thành công');
	}
	public function Sua_doc_gia($ma_doc_gia)
	{
		$nganh= nganh::all();
		$doc_gia = doc_gia::find($ma_doc_gia);
		return view('admin/doc_gia/sua_doc_gia',[

			'nganh'=> $nganh,
			'doc_gia'=>$doc_gia 
			
		]);
	}
	public function postSua_doc_gia(Request $request,$ma_doc_gia)
	{
		$this->validate($request, [
			'ma_nganh'=>'required',
			'ten_doc_gia' =>'required|min:3',
			'email' =>'required|email',
			'dia_chi' => 'required|min:5|max:100',
			'ngay_sinh' => 'required',
			'sdt' => 'required',
			'gioi_tinh' => 'required',
			'cap_do' => 'required',
			
		],
		[
			'ma_nganh.required' =>'Bạn chưa chọn tên ngành ',
			'ten_doc_gia.required' =>'Bạn chưa nhập tên ',
			'ten_doc_gia.min' =>'Tên người dùng phải có ít nhất 3 kí tự',

			'email.required' =>'Bạn chưa nhập Email',
			'email.email' =>'Bạn chưa nhập Đúng Email',
			
			'dia_chi.min.required' =>'Bạn chưa nhập địa chỉ',
			'dia_chi.min' =>'Địa chỉ của bạn phải có ít nhất 3 kí tự',
			'ngay_sinh.required' =>'Bạn chưa nhập ngày sinh',
			'sdt.required' =>'Bạn chưa nhập số điện thoại ',	
			'gioi_tinh.required' =>'Bạn chưa nhập giới tính',
			'cap_do.required' =>'Bạn chưa nhập cấp độ',
			
		]);
		$dg = doc_gia::find($ma_doc_gia);
		$dg->ma_nganh = $request->nganh;
		$dg->ten_doc_gia = $request->ten_doc_gia;
		$dg->email = $request->email;
		$dg->dia_chi = $request->dia_chi;
		$dg->ngay_sinh= $request->ngay_sinh;
		
		$dg->gioi_tinh = $request->gioi_tinh;
		$dg->cap_do = $request->cap_do;
		$dg->sdt = $request->sdt;
		$dg->save();

		return redirect('tong/doc_gia/sua_doc_gia/'.$ma_doc_gia)->with('thongbao','sửa thành công');
	}

	public function Xoa_doc_gia($id)
	{
		$dg = doc_gia::find($id);
		$dg->delete();
		return redirect('tong/doc_gia/danh_sach_doc_gia')->with('thongbao','xóa thành công');
	}

	public function Dang_nhap_doc_gia()
	{
		return view('doc_gia/dang_nhap_doc_gia');
	}
	public function postDang_nhap_doc_gia(Request $request)
	{
		$this->validate($request, [
			
			'email' =>'required',
			
			
		],
		[

			'email.required' =>'Bạn chưa nhập Email',
			
			
			
		]);

		if(Auth::attempt(['email'=>$request->email]))
		{
			return redirect('tong/sach/danh_sach_sach_dg');
			
		}	
		else
		{
			return redirect('doc_gia/dang_nhap_doc_gia')->with('thongbao','đăng nhập không thành công');
		}
	}
	public function getLogout() {
		Auth::logout();
		return redirect('doc_gia/dang_nhap_doc_gia');
	}

}

