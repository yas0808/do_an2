<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\sach;
use App\the_loai_sach;
use App\tac_gia;
use App\nha_xuat_ban;



class Sach_Controller extends Controller
{
     public function Danh_sach_sach()
	{
		$sach= sach::all();
		return view('admin/sach/danh_sach_sach', ['sach'=> $sach]);
	}
	public function Them_sach()
	{	
		$the_loai_sach= the_loai_sach::all();
		$tac_gia= tac_gia::all();
		$nha_xuat_ban= nha_xuat_ban::all();
		return view('admin/sach/them_sach',[
			'the_loai_sach'=> $the_loai_sach, 
			'tac_gia'=> $tac_gia,
			'nha_xuat_ban'=> $nha_xuat_ban
		]);
	}
	public function postThem_sach(Request $request)

	{
		$this->validate($request, [
			'the_loai_sach'=>'required',
			'tac_gia'=>'required',
			'nha_xuat_ban'=>'required',
			'ten_sach' =>'required|min:3',
			'gioi_thieu' =>'required|min:10',
			'so_luong' => 'required|min:1',
			'ngay_nhap' => 'required',
			'gia_tien' => 'required',
			
		],
		[
			'the_loai_sach.required' =>'Bạn chưa chọn tên thể loại sách ',
			'tac_gia.required' =>'Bạn chưa chọn tên tác giả ',
			'nha_xuat_ban.required' =>'Bạn chưa chọn tên nhà xuất bản ',
			'ten_sach.required' =>'Bạn chưa nhập tên ',
			'ten_sach.min' =>'Tên sách phải có ít nhất 3 kí tự',
			'gioi_thieu.min.required' =>'Bạn chưa nhập giới thiệu',
			'gioi_thieu.min' =>'Giới thiệu của bạn phải có ít nhất 3 kí tự',
			'ngay_nhap.required' =>'Bạn chưa nhập ngày nhập kho',
			'gia_tien.required' =>'Bạn chưa nhập giá tiền',
			
		]);
		$sh = new sach;
		$sh->ma_the_loai_sach = $request->the_loai_sach;
		$sh->ma_tac_gia = $request->tac_gia;
		$sh->ma_nha_xuat_ban = $request->nha_xuat_ban;
		$sh->ten_sach = $request->ten_sach;
		$sh->gioi_thieu = $request->gioi_thieu;
		$sh->so_luong = $request->so_luong;
		$sh->ngay_nhap= $request->ngay_nhap;
		$sh->gia_tien = $request->gia_tien;
		$sh->save();

		return redirect('tong/sach/them_sach')->with('thongbao','Thêm thành công');
	}
	public function Sua_sach($ma_sach)
	{
		$the_loai_sach= the_loai_sach::all();
		$tac_gia= tac_gia::all();
		$nha_xuat_ban= nha_xuat_ban::all();
		$sach = sach::find($ma_sach);
		return view('admin/sach/sua_sach',[
			'the_loai_sach'=> $the_loai_sach, 
			'tac_gia'=> $tac_gia,
			'nha_xuat_ban'=> $nha_xuat_ban,
			'sach'=>$sach
		]);
	}
	public function postSua_sach(Request $request,$ma_sach)
	{
		$this->validate($request, [
			'ma_the_loai_sach'=>'required',
			'ma_tac_gia'=>'required',
			'ma_nha_xuat_ban'=>'required',
			'ten_sach' =>'required|min:3',
			'gioi_thieu' =>'required|min:10',
			'so_luong' => 'required|min:1',
			'ngay_nhap' => 'required',
			'gia_tien' => 'required',
			
		],
		[
			'ma_the_loai_sach.required' =>'Bạn chưa chọn tên thể loại sách ',
			'ma_tac_gia.required' =>'Bạn chưa chọn tên tác giả ',
			'ma_nha_xuat_ban.required' =>'Bạn chưa chọn tên nhà xuất bản ',
			'ten_sach.required' =>'Bạn chưa nhập tên ',
			'ten_sach.min' =>'Tên sách phải có ít nhất 3 kí tự',
			'gioi_thieu.min.required' =>'Bạn chưa nhập giới thiệu',
			'gioi_thieu.min' =>'Giới thiệu của bạn phải có ít nhất 3 kí tự',
			'ngay_nhap.required' =>'Bạn chưa nhập ngày nhập kho',
			'gia_tien.required' =>'Bạn chưa nhập giá tiền',
			
		]);
		$sh = sach::find($ma_sach);
		$sh->ma_the_loai_sach = $request->ma_the_loai_sach;
		$sh->ma_nha_xuat_ban = $request->ma_nha_xuat_ban;
		$sh->ten_sach = $request->ten_sach;
		$sh->ma_tac_gia = $request->ma_tac_gia;
		$sh->gioi_thieu = $request->gioi_thieu;
		$sh->so_luong = $request->so_luong;
		$sh->ngay_nhap= $request->ngay_nhap;
		$sh->gia_tien = $request->gia_tien;
		$sh->save();


		return redirect('tong/sach/sua_sach/'.$ma_sach)->with('thongbao','sửa thành công');
	}

	public function Xoa_sach($id)
	{
		$sh = sach::find($id);
		$sh->delete();
		return redirect('tong/sach/danh_sach_sach')->with('thongbao','xóa thành công');
	}
}
