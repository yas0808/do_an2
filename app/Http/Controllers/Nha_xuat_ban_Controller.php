<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\nha_xuat_ban;
class Nha_xuat_ban_Controller extends Controller
{
     public function Danh_sach_nha_xuat_ban()
	{
		$nha_xuat_ban = nha_xuat_ban::all();
		return view('admin/nha_xuat_ban/danh_sach_nha_xuat_ban', ['nha_xuat_ban'=> $nha_xuat_ban]);
	}
	public function Them_nha_xuat_ban()
	{
		return view('admin/nha_xuat_ban/them_nha_xuat_ban');
	}
	public function postThem_nha_xuat_ban(Request $request)
	{
		$this->validate($request, [
			'ten_nha_xuat_ban' =>'required|min:3',
			
			
		],
		[
			'ten_nha_xuat_ban.required' =>'Bạn chưa nhập tên nhà sản xuất ',
			'ten_nha_xuat_ban.min' =>'Tên nhà sản xuất phải có ít nhất 3 kí tự',
			
			
		]);
		$nha_xuat_ban = new nha_xuat_ban;
		$nha_xuat_ban->ten_nha_xuat_ban = $request->ten_nha_xuat_ban;
		
		$nha_xuat_ban->save();

		return redirect('tong/nha_xuat_ban/them_nha_xuat_ban')->with('thongbao','Thêm thành công');
	}
	public function Sua_nha_xuat_ban($ma_nha_xuat_ban)
	{
		$nha_xuat_ban = nha_xuat_ban::find($ma_nha_xuat_ban);
		return view('admin/nha_xuat_ban/sua_nha_xuat_ban',['nha_xuat_ban' => $nha_xuat_ban]);
	}
	public function postSua_nha_xuat_ban(Request $request,$ma_nha_xuat_ban)
	{
		$this->validate($request, [
			'ten_nha_xuat_ban' =>'required|min:3',
			
			
		],
		[
			'ten_nha_xuat_ban.required' =>'Bạn chưa nhập tên nhà sản xuất ',
			'ten_nha_xuat_ban.min' =>'Tên nhà sản xuất phải có ít nhất 3 kí tự',
			
			
		]);
		$nha_xuat_ban = nha_xuat_ban::find($ma_nha_xuat_ban);
		$nha_xuat_ban->ten_nha_xuat_ban = $request->ten_nha_xuat_ban;
		
		$nha_xuat_ban->save();


		return redirect('tong/nha_xuat_ban/sua_nha_xuat_ban/'.$ma_nha_xuat_ban)->with('thongbao','sửa thành công');
	}

	public function Xoa_nha_xuat_ban($id)
	{
		$nha_xuat_ban = nha_xuat_ban::find($id);
		$nha_xuat_ban->delete();
		return redirect('tong/nha_xuat_ban/danh_sach_nha_xuat_ban')->with('thongbao','xóa thành công');
	}
}
