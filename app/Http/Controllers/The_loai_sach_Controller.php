<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\the_loai_sach;
class The_loai_sach_Controller extends Controller
{
    public function Danh_sach_the_loai_sach()
	{
		$the_loai_sach= the_loai_sach::all();
		return view('admin/the_loai_sach/danh_sach_the_loai_sach', ['the_loai_sach'=> $the_loai_sach]);
	}
	public function Them_the_loai_sach()
	{
		return view('admin/the_loai_sach/them_the_loai_sach');
	}
	public function postThem_the_loai_sach(Request $request)
	{
		$this->validate($request, [
			'ten_the_loai_sach' =>'required|min:3',
			
			
		],
		[
			'ten_the_loai_sach.required' =>'Bạn chưa nhập tên thể loại sách ',
			'ten_the_loai_sach.min' =>'Tên thể loại sách phải có ít nhất 3 kí tự',
			
			
		]);
		$the_loai_sach = new the_loai_sach;
		$the_loai_sach->ten_the_loai_sach = $request->ten_the_loai_sach;
		
		$the_loai_sach->save();

		return redirect('tong/the_loai_sach/them_the_loai_sach')->with('thongbao','Thêm thành công');
	}
	public function Sua_the_loai_sach($ma_the_loai_sach)
	{
		$the_loai_sach = the_loai_sach::find($ma_the_loai_sach);
		return view('admin/the_loai_sach/sua_the_loai_sach',['the_loai_sach' => $the_loai_sach]);
	}
	public function postSua_the_loai_sach(Request $request,$ma_the_loai_sach)
	{
		$this->validate($request, [
			'ten_the_loai_sach' =>'required|min:3',
			
			
		],
		[
			'ten_the_loai_sach.required' =>'Bạn chưa nhập tên thể loại sách ',
			'ten_the_loai_sach.min' =>'Tên thể loại sách phải có ít nhất 3 kí tự',
			
			
		]);
		$the_loai_sach = the_loai_sach::find($ma_the_loai_sach);
		$the_loai_sach->ten_the_loai_sach = $request->ten_the_loai_sach;
		
		$the_loai_sach->save();


		return redirect('tong/the_loai_sach/sua_the_loai_sach/'.$ma_the_loai_sach)->with('thongbao','sửa thành công');
	}

	public function Xoa_the_loai_sach($id)
	{
		$the_loai_sach = the_loai_sach::find($id);
		$the_loai_sach->delete();
		return redirect('tong/the_loai_sach/danh_sach_the_loai_sach')->with('thongbao','xóa thành công');
	}
}
