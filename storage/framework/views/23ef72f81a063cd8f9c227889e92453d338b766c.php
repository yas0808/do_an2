<?php $__env->startSection('content'); ?>
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Sửa đặt mượn
                            <small></small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <?php if(count($errors) > 0): ?>
                        <div class="alert alert-danger">
                            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $err): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php echo e($err); ?><br>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <?php endif; ?>

                        <?php if(session('thongbao')): ?>
                            <div class="alert alert-success">
                                <?php echo e(session('thongbao')); ?>

                            </div>
                        <?php endif; ?>
                        <form action="tong/dat_muon/sua_dat_muon/<?php echo e($dat_muon->ma_dat_muon); ?>" method="POST"/>
                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                            <div class="form-group">
                                <label>Tên Độc Giả</label>
                               <select class="form-control" name="doc_gia">
                              <?php $__currentLoopData = $doc_gia; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dg): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($dg->ma_doc_gia); ?>">
                                    <?php echo e($dg->ten_doc_gia); ?>

                                </option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
                            </select>
                            </div>
                            <div class="form-group">
                                <label>Tên Sách</label>
                               <select class="form-control" name="sach">
                              <?php $__currentLoopData = $sach; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sh): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($sh->ma_sach); ?>">
                                    <?php echo e($sh->ten_sach); ?>

                                </option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
                            </select>
                            </div>
                            <div class="form-group">
                                <label>Ngày Đặt Mượn</label>
                                <input class="form-control" type = "date" name="ngay_dat" placeholder="Nhập ngày đặt mượn"  value="<?php echo e($dat_muon->ngay_dat); ?>"  />
                            </div>
                            <div class="form-group">
                                <label>Số Lượng</label>
                                <input class="form-control" type = "number" name="so_luong" placeholder="Nhập số Lượng"  value="<?php echo e($dat_muon->so_luong); ?>" />
                            </div>
                            <div class="form-group">
                                <label>Trạng Thái</label>
                                <label class="radio-inline">
                                    <input name="trang_thai" value="0" checked="" type="radio">Đã Trả
                                </label>
                                <label class="radio-inline">
                                    <input name="cap_do" value="1" type="radio">Chưa Trả
                                </label>
                            </div>
                        <button type="submit" class="btn btn-default">Sửa</button>
                        <button type="reset" class="btn btn-default">Làm mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
<?php $__env->stopSection(); ?>




<?php echo $__env->make('admin.layout.index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\do_an_2\resources\views/admin/dat_muon/sua_dat_muon.blade.php ENDPATH**/ ?>