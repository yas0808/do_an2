<?php $__env->startSection('content'); ?>
 <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Tác Giả
                            <small>Danh sách Tác Giả</small>
                        </h1>
                    </div>
                    <br>
                    <!-- /.col-lg-12 -->
                    <div>
                        <p>
                        <?php if(session('thongbao')): ?>
                            <div class="alert alert-success">
                                <?php echo e(session('thongbao')); ?>

                            </div>
                        <?php endif; ?>
                    </p>
                    </div>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>Mã Tác Giả</th>
                                <th>Tên Tác Giả</th>   
                                <th>Giới Thiệu</th>                             
                                <th>Sửa</th>
                                <th>Xóa</th>
                            </tr>
                        </thead>
                        <tbody>
                           <?php $__currentLoopData = $tac_gia; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tg): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr class="even gradeC" align="center">
                                <td><?php echo e($tg->ma_tac_gia); ?></td>
                                <td><?php echo e($tg->ten_tac_gia); ?></td>
                                <td><?php echo e($tg->gioi_thieu); ?></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="tong/tac_gia/sua_tac_gia/<?php echo e($tg->ma_tac_gia); ?>"> Edit</a></td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="tong/tac_gia/xoa_tac_gia/<?php echo e($tg->ma_tac_gia); ?>">Delete</a></td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\do_an_2\resources\views/admin/tac_gia/danh_sach_tac_gia.blade.php ENDPATH**/ ?>