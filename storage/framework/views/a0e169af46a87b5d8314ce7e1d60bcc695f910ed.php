<?php $__env->startSection('content'); ?>
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Sửa Sách
                            <small><?php echo e($sach -> ten_sach); ?></small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <?php if(session('thongbao')): ?>
                            <div class="alert alert-success">
                                <?php echo e(session('thongbao')); ?>

                            </div>
                        <?php endif; ?>
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <?php if(count($errors) > 0): ?>
                        <div class="alert alert-danger">
                            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $err): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php echo e($err); ?><br>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <?php endif; ?>

                        <?php if(session('thongbao')): ?>
                            <div class="alert alert-success">
                                <?php echo e(session('thongbao')); ?>

                            </div>
                        <?php endif; ?>
                        <form action="tong/sach/sua_sach/<?php echo e($sach->ma_sach); ?>" method="POST"/>
                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                           <div class="form-group">
                            <label>Thể Loại Sách</label>
                            <select class="form-control" name="ma_the_loai_sach">
                            <?php $__currentLoopData = $the_loai_sach; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tl): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($sach->ma_the_loai_sach == $tl->ma_sach): ?>
                                <?php echo e("selected"); ?>

                                <?php endif; ?>
                                <option value="<?php echo e($tl->ma_the_loai_sach); ?>">
                                    <?php echo e($tl->ten_the_loai_sach); ?>

                                </option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
                            </select>
                        </div>

                        <div class="form-group">

                            <label>Tác Giả</label>
                            <select class="form-control" name="ma_tac_gia">
                              <?php $__currentLoopData = $tac_gia; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tg): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($sach->ma_tac_gia == $tg->ma_sach): ?>
                                <?php echo e("selected"); ?>

                                <?php endif; ?>
                                <option value="<?php echo e($tg->ma_tac_gia); ?>">
                                    <?php echo e($tg->ten_tac_gia); ?>

                                </option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <label>Nhà Xuất Bản</label>
                            <select class="form-control" name="ma_nha_xuat_ban">
                              <?php $__currentLoopData = $nha_xuat_ban; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $nxb): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($sach->ma_nha_xuat_ban == $nxb->ma_sach): ?>
                                    <?php echo e("selected"); ?>

                                <?php endif; ?>
                                <option value="<?php echo e($nxb->ma_nha_xuat_ban); ?>">
                                    <?php echo e($nxb->ten_nha_xuat_ban); ?>

                                </option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
                            </select>
                        </div>
                            <div class="form-group">
                                <label>Tên Sách</label>
                                <input class="form-control" name="ten_sach" placeholder="Nhập tên sách" value="<?php echo e($sach->ten_sach); ?>" />
                            </div>
                            <div class="form-group">
                                <label>Giới Thiệu</label>
                                <input class="form-control" name="gioi_thieu" placeholder="Nhập giới thiệu" value="<?php echo e($sach->gioi_thieu); ?>" />
                            </div>
                             <div class="form-group">
                                <label>Số Lượng</label>
                                <input type="number" class="form-control " name="so_luong" placeholder="Nhập số lượng"
                                 value="<?php echo e($sach->so_luong); ?>"/>
                            </div>
                             <div class="form-group">
                                <label>Ngày Nhập</label>
                                <input type = "date"class="form-control" name="ngay_nhap" placeholder="Nhập ngày lưu kho" value="<?php echo e($sach->ngay_nhap); ?>"/>
                            </div>
                             <div class="form-group">
                                <label>Giá Tiền</label>
                                <input type="number" class="form-control" name="gia_tien" placeholder="Nhập số tiền"
                                value="<?php echo e($sach->gia_tien); ?>" />
                            </div>
                           
                            
                            <button type="submit" class="btn btn-default">Sửa</button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
<?php $__env->stopSection(); ?>




<?php echo $__env->make('admin.layout.index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\do_an_2\resources\views/admin/sach/sua_sach.blade.php ENDPATH**/ ?>