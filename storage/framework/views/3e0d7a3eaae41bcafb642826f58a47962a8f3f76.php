<?php $__env->startSection('content'); ?>
 <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Admin
                            <small>Danh sách admin</small>
                        </h1>
                    </div>
                    <br>
                    <!-- /.col-lg-12 -->
                    <div>
                        <p>
                        <?php if(session('thongbao')): ?>
                            <div class="alert alert-success">
                                <?php echo e(session('thongbao')); ?>

                            </div>
                        <?php endif; ?>
                    </p>
                    </div>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>Mã admin</th>
                                <th>Tên admin</th>
                                <th>Email</th>
                                <th>Mật khẩu</th>
                                <th>Địa chỉ</th>
                                <th>Ngày sinh</th>
                                <th>Giới tính</th>
                                <th>SĐT</th>
                                <th>Cấp độ</th>
                                
                                <th>Sửa</th>
                                <th>Xóa</th>
                            </tr>
                        </thead>
                        <tbody>
                           <?php $__currentLoopData = $admin; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ad): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr class="even gradeC" align="center">
                                <td><?php echo e($ad->ma_admin); ?></td>
                                <td><?php echo e($ad->ten_admin); ?></td>
                                <td><?php echo e($ad->email); ?></td>
                                <td><?php echo e($ad->password); ?></td>
                                <td><?php echo e($ad->dia_chi); ?></td>
                                <td><?php echo e($ad->ngay_sinh); ?></td>
                                <td><?php if($ad->gioi_tinh == 1): ?>
                                    <?php echo e("Nữ"); ?>

                                    <?php else: ?>
                                    <?php echo e("Nam"); ?>

                                    <?php endif; ?></td>
                                <td><?php echo e($ad->sdt); ?></td>
                                <td>
                                    <?php if($ad->cap_do == 1): ?>
                                    <?php echo e("Admin"); ?>

                                    <?php else: ?>
                                    <?php echo e("SuperAdmin"); ?>

                                    <?php endif; ?>
                                </td>
                                
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="tong/admin/sua_admin/<?php echo e($ad->ma_admin); ?>"> Edit</a></td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="tong/admin/xoa_admin/<?php echo e($ad->ma_admin); ?>">Delete</a></td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\do_an_2\resources\views/admin/use/danh_sach_admin.blade.php ENDPATH**/ ?>