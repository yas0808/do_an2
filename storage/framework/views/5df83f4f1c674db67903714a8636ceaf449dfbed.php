<?php $__env->startSection('content'); ?>
 <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Ngành
                            <small>Danh sách ngành</small>
                        </h1>
                    </div>
                    <br>
                    <!-- /.col-lg-12 -->
                    <div>
                        <p>
                        <?php if(session('thongbao')): ?>
                            <div class="alert alert-success">
                                <?php echo e(session('thongbao')); ?>

                            </div>
                        <?php endif; ?>
                    </p>
                    </div>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>Mã Ngành</th>
                                <th>Tên Ngành</th>                             
                                <th>Sửa</th>
                                <th>Xóa</th>
                            </tr>
                        </thead>
                        <tbody>
                           <?php $__currentLoopData = $nganh; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $nganh): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr class="even gradeC" align="center">
                                <td><?php echo e($nganh->ma_nganh); ?></td>
                                <td><?php echo e($nganh->ten_nganh); ?></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"><a href="tong/nganh/sua_nganh/<?php echo e($nganh->ma_nganh); ?>"> Edit</a></td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i></i> <a href="tong/nganh/xoa_nganh/<?php echo e($nganh->ma_nganh); ?>">Delete</a></td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\do_an_2\resources\views/admin/nganh/danh_sach_nganh.blade.php ENDPATH**/ ?>