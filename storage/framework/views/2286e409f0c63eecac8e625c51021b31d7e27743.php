<?php $__env->startSection('content'); ?>
<!-- Page Content -->
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <style type="text/css">
    .form{width: 650px}
            .main-password {position: relative;}
            .icon-view{position: absolute; right: 12px; top: 36px;}
               
</style>
</head>
<body>


<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Đổi Mật Khẩu
                    <small></small>
                </h1>
            </div>
            <div class="container mt-5">
                <form class="form">
                   <div class="form-group" style="position: relative;"> 
                        <div class="main-password">
                            <label>Mật Khẩu Cũ</label>
                            <input type="text" class="form-control input-password" aria-label="password">
                            <a style="position: absolute; top: 54%; right: 10px" href="JavaScript:;void(0)" class="icon-view">
                               
                            </a>
                        </div>
                    </div>
                    <div class="form-group " style="position: relative;">
                        <div class="main-password">
                             <label>Mật Khẩu mới</label>
                            <input type="text" class="form-control input-password" aria-label="password">
                            <a style="position: absolute; top: 54%; right: 10px" href="JavaScript:void(0" class="icon-view">
                                
                            </a>
                        </div>
                    </div>  
                    <div class="form-group" style="position: relative;">
                        <div class="main-password">
                             <label>Nhập Lại Mật Khẩu </label>
                            <input type="text" class="form-control input-password" aria-label="password">
                            <a style="position: absolute; top: 54%; right: 10px" href="JavaScript:void(0);" class="icon-view">
                               
                            </a>
                        </div>
                    </div> 
                    
                </form>

            </div>
            <?php $__env->stopSection(); ?>
</body>
</html>
<script type="text/javascript">
                $('.main-password').find('.input-password').each(function(index, input) {
                    var $input = $(input);
                    $input.parent().find('.icon-view').click(function() {
                        var change = "";
                        if ($(this).find('i').hasClass('fa-eye')) {
                            $(this).find('i').removeClass('fa-eye')
                            $(this).find('i').addClass('fa-eye-slash')
                            change = "text";
                        } else {
                            $(this).find('i').removeClass('fa-eye-slash')
                            $(this).find('i').addClass('fa-eye')
                            change = "password";
                        }
                        var rep = $("<input type='" + change + "' />")
                            .attr('id', $input.attr('id'))
                            .attr('name', $input.attr('name'))
                            .attr('class', $input.attr('class'))
                            .val($input.val())
                            .insertBefore($input);
                        $input.remove();
                        $input = rep;
                    }).insertAfter($input);
                });
            });
</script>
<?php echo $__env->make('admin.layout.index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\do_an_2\resources\views/admin/mat_khau/doi_mat_khau.blade.php ENDPATH**/ ?>