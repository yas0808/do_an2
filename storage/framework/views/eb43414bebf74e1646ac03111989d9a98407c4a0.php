<?php $__env->startSection('content'); ?>
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Sách
                            <small>Thêm Sách</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <?php if(count($errors) > 0): ?>
                        <div class="alert alert-danger">
                            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $err): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php echo e($err); ?><br>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <?php endif; ?>

                        <?php if(session('thongbao')): ?>
                            <div class="alert alert-success">
                                <?php echo e(session('thongbao')); ?>

                            </div>
                        <?php endif; ?>
                        <form action="tong/sach/them_sach" method="POST"/>
                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

                        <div class="form-group">
                            <label>Thể Loại Sách</label>
                            <select class="form-control" name="the_loai_sach">
                              <?php $__currentLoopData = $the_loai_sach; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tl): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($tl->ma_the_loai_sach); ?>">
                                    <?php echo e($tl->ten_the_loai_sach); ?>

                                </option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Tác Giả</label>
                            <select class="form-control" name="tac_gia">
                              <?php $__currentLoopData = $tac_gia; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tg): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($tg->ma_tac_gia); ?>">
                                    <?php echo e($tg->ten_tac_gia); ?>

                                </option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <label>Nhà Xuất Bản</label>
                            <select class="form-control" name="nha_xuat_ban">
                              <?php $__currentLoopData = $nha_xuat_ban; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $nxb): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($nxb->ma_nha_xuat_ban); ?>">
                                    <?php echo e($nxb->ten_nha_xuat_ban); ?>

                                </option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
                            </select>
                        </div>
                        <div class="form-group">
                                <label>Tên Sách</label>
                                <input class="form-control" name="ten_sach" placeholder="Nhập tên sach" />
                        </div>
                        <div class="form-group">
                                <label>Giới Thiệu </label>
                                <input class="form-control" name="gioi_thieu" placeholder="Nhập giới thiệu " />
                            </div>
                            <div class="form-group">
                                <label>Số Lượng</label>
                                <input class="form-control" type="number" name="so_luong" placeholder="Nhập số lượng" />
                            </div>
                            <div class="form-group">
                                <label>Ngày Nhập</label>
                                <input class="form-control" type="date" name="ngay_nhap" placeholder="Nhập ngày nhập" />
                            </div>
                            <div class="form-group">
                                <label>Giá Tiền</label>
                                <input class="form-control" type="number" name="gia_tien" placeholder="Nhập giá tiền" />
                            </div>
                            <button type="submit" class="btn btn-default">Thêm</button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\do_an_2\resources\views/admin/sach/them_sach.blade.php ENDPATH**/ ?>