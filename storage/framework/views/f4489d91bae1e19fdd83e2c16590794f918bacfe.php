<?php $__env->startSection('content'); ?>
 <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Đơn Đặt Mượn
                            <small>Danh sách Đơn Đặt Mượn</small>
                        </h1>
                    </div>
                    <br>
                    <!-- /.col-lg-12 -->
                    <div>
                        <p>
                        <?php if(session('thongbao')): ?>
                            <div class="alert alert-success">
                                <?php echo e(session('thongbao')); ?>

                            </div>
                        <?php endif; ?>
                    </p>
                    </div>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                
                                <th>Sách</th>
                                <th>Số Lượng</th>
                                <th>Trạng Thái</th>
                                
                                <th>Sửa</th>
                                <th>Xóa</th>
                            </tr>
                        </thead>
                        <tbody>
                           <?php $__currentLoopData = $don_dat_muon; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ddm): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr class="even gradeC" align="center">
                              
                                <td><?php echo e($ddm->sach->ten_sach); ?></td>
                                <td><?php echo e($ddm->so_luong); ?></td>
                                <td><?php echo e($ddm->trang_thai); ?></td>
                                    
                                
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="tong/don_dat_muon/sua_don_dat_muon/<?php echo e($ddm->ma_dat_muon); ?>"> Edit</a></td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="tong/don_dat_muon/xoa_don_dat_muon/<?php echo e($ddm->ma_don_dat_muon); ?>">Delete</a></td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\do_an_2\resources\views/admin/don_dat_muon/danh_sach_don_dat_muon.blade.php ENDPATH**/ ?>