<?php $__env->startSection('content'); ?>
 <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Nhà Xuất Bản
                            <small>Danh sách nhà xuất bản</small>
                        </h1>
                    </div>
                    <br>
                    <!-- /.col-lg-12 -->
                    <div>
                        <p>
                        <?php if(session('thongbao')): ?>
                            <div class="alert alert-success">
                                <?php echo e(session('thongbao')); ?>

                            </div>
                        <?php endif; ?>
                    </p>
                    </div>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>Mã nhà xuất bản</th>
                                <th>Tên nhà xuất bản</th>                             
                                <th>Sửa</th>
                                <th>Xóa</th>
                            </tr>
                        </thead>
                        <tbody>
                           <?php $__currentLoopData = $nha_xuat_ban; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $xb): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr class="even gradeC" align="center">
                                <td><?php echo e($xb->ma_nha_xuat_ban); ?></td>
                                <td><?php echo e($xb->ten_nha_xuat_ban); ?></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="tong/nha_xuat_ban/sua_nha_xuat_ban/<?php echo e($xb->ma_nha_xuat_ban); ?>"> Edit</a></td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="tong/nha_xuat_ban/xoa_nha_xuat_ban/<?php echo e($xb->ma_nha_xuat_ban); ?>">Delete</a></td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\do_an_2\resources\views/admin/nha_xuat_ban/danh_sach_nha_xuat_ban.blade.php ENDPATH**/ ?>