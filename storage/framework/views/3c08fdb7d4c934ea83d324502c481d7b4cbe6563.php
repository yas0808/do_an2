<?php $__env->startSection('content'); ?>
 <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Sách 
                            <small>Kho Sách</small>
                        </h1>
                    </div>
                    <br>
                    <!-- /.col-lg-12 -->
                    <div>
                        <p>
                        <?php if(session('thongbao')): ?>
                            <div class="alert alert-success">
                                <?php echo e(session('thongbao')); ?>

                            </div>
                        <?php endif; ?>
                    </p>
                    </div>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                
                                <th>Thể loại sách</th>
                                <th>Tác Giả</th>
                                <th>Nhà Xuất Bản</th>
                                <th>Tên Sách</th>
                                <th>Giới Thiệu</th>
                                <th>Số Lượng</th>
                                <th>Ngày Nhập</th>
                                <th>Giá Tiền</th>
                                
                                <th>Sửa</th>
                                <th>Xóa</th>
                            </tr>
                        </thead>
                        <tbody>
                           <?php $__currentLoopData = $sach; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sh): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr class="even gradeC" align="center">
                                <td><?php echo e($sh->the_loai_sach->ten_the_loai_sach); ?></td>
                                <td><?php echo e($sh->tac_gia->ten_tac_gia); ?></td>
                                <td><?php echo e($sh->nha_xuat_ban['ten_nha_xuat_ban']); ?></td>
                                <td><?php echo e($sh->ten_sach); ?></td>
                                <td><?php echo e($sh->gioi_thieu); ?></td>
                                <td><?php echo e($sh->so_luong); ?></td>
                                <td><?php echo e($sh->ngay_nhap); ?></td>
                                <td>
                                    <?php echo number_format($sh->gia_tien); ?>
                                </td>
                                
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="tong/sach/sua_sach/<?php echo e($sh->ma_sach); ?>"> Edit</a></td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="tong/sach/xoa_sach/<?php echo e($sh->ma_sach); ?>">Delete</a></td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\do_an_2\resources\views/admin/sach/danh_sach_sach.blade.php ENDPATH**/ ?>