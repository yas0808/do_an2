<?php $__env->startSection('content'); ?>
 <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Độc Giả
                            <small>Danh Sách Độc Gỉa</small>
                        </h1>
                    </div>
                    <br>
                    <!-- /.col-lg-12 -->
                    <div>
                        <p>
                        <?php if(session('thongbao')): ?>
                            <div class="alert alert-success">
                                <?php echo e(session('thongbao')); ?>

                            </div>
                        <?php endif; ?>
                    </p>
                    </div>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>Ngành</th>
                                <th>Tên độc giả</th>
                                <th>Email</th>
                                <th>Địa chỉ</th>
                                <th>Ngày sinh</th>
                                <th>Giới tính</th>
                               
                                <th>Cấp độ</th>
                                 <th>SĐT</th>
                                <th>Sửa</th>
                                <th>Xóa</th>
                            </tr>
                        </thead>
                        <tbody>
                           <?php $__currentLoopData = $doc_gia; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dg): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr class="even gradeC" align="center">
                                
                                <td><?php echo e($dg->nganh->ten_nganh); ?></td>
                                <td><?php echo e($dg->ten_doc_gia); ?></td>
                                <td><?php echo e($dg->email); ?></td>
                                
                                <td><?php echo e($dg->dia_chi); ?></td>
                                <td><?php echo e($dg->ngay_sinh); ?></td>
                                <td><?php if($dg->gioi_tinh == 1): ?>
                                    <?php echo e("Nữ"); ?>

                                    <?php else: ?>
                                    <?php echo e("Nam"); ?>

                                    <?php endif; ?></td>
                              
                                <td>
                                    <?php if($dg->cap_do == 1): ?>
                                    <?php echo e("sinh viên"); ?>

                                    <?php else: ?>
                                    <?php echo e("Giảng Viên"); ?>

                                    <?php endif; ?>
                                </td>
                                <td>
                                    <?php echo e($dg->sdt); ?>

                                </td>
                                
                                 <td class="center"><i class="fa fa-pencil fa-fw"></i><a href="tong/doc_gia/sua_doc_gia/<?php echo e($dg->ma_doc_gia); ?>"> Edit</a></td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i> <a href="tong/doc_gia/xoa_doc_gia/<?php echo e($dg->ma_doc_gia); ?>">Delete</a></td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\do_an_2\resources\views/admin/doc_gia/danh_sach_doc_gia.blade.php ENDPATH**/ ?>