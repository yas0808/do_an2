<?php $__env->startSection('content'); ?>
 <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Đặt Mượn
                            <small>Danh Sách Đặt Mượn</small>
                        </h1>
                    </div>
                    <br>
                    <!-- /.col-lg-12 -->
                    <div>
                        <p>
                        <?php if(session('thongbao')): ?>
                            <div class="alert alert-success">
                                <?php echo e(session('thongbao')); ?>

                            </div>
                        <?php endif; ?>
                    </p>
                    </div>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>Mã Đặt Mượn</th>
                                <th>Tên Sách</th>
                                <th>Tên Độc Giả</th>
                                <th>Ngày Đặt</th>
                                <th>Trạng Thái</th>
                                <th>Số Lượng</th>
                                <th>Sửa</th>
                                <th>Xóa</th>
                            </tr>
                        </thead>
                        <tbody>
                           <?php $__currentLoopData = $dat_muon; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dm): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr class="even gradeC" align="center">
                                <td><?php echo e($dm->ma_dat_muon); ?></td>
                                <td><?php echo e($dm->sach->ten_sach); ?></td>
                                <td><?php echo e($dm->doc_gia->ten_doc_gia); ?></td>
                                <td><?php echo e($dm->ngay_dat); ?></td>
                                <td><?php if($dm->trang_thai == 1): ?>
                                    <?php echo e("Đã Trả"); ?>

                                    <?php else: ?>
                                    <?php echo e("Chưa Trả"); ?>

                                    <?php endif; ?></td>
                                <td><?php echo e($dm->so_luong); ?></td>
                                
                                <td class="center"><i class="fa fa-pencil fa-fw"></i><a href="tong/dat_muon/sua_dat_muon/<?php echo e($dm->ma_dat_muon); ?>"> Edit</a></td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i> <a href="tong/dat_muon/xoa_dat_muon/<?php echo e($dm->ma_dat_muon); ?>">Delete</a></td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\do_an_2\resources\views/admin/dat_muon/danh_sach_dat_muon.blade.php ENDPATH**/ ?>