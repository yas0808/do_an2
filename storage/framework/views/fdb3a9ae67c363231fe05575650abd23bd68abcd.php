<?php $__env->startSection('content'); ?>
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Admin
                            <small>Thêm Admin</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <?php if(count($errors) > 0): ?>
                        <div class="alert alert-danger">
                            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $err): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php echo e($err); ?><br>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <?php endif; ?>

                        <?php if(session('thongbao')): ?>
                            <div class="alert alert-success">
                                <?php echo e(session('thongbao')); ?>

                            </div>
                        <?php endif; ?>
                        <form action="tong/admin/them_admin" method="POST"/>
                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                            <div class="form-group">
                                <label>Tên Admin</label>
                                <input class="form-control" name="ten_admin" placeholder="Nhập tên admin" />
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" name="email" placeholder="Nhập email" />
                            </div>
                            <div class="form-group">
                                <label>Mật Khẩu</label>
                                <input type = "password"class="form-control" name="password" placeholder="Nhập mật khẩu" />
                            </div>
                             <div class="form-group">
                                <label>Nhập lại Mật Khẩu</label>
                                <input type = "password"class="form-control" name="passwordAgain" placeholder="Nhập mật lại khẩu" />
                            </div>
                             <div class="form-group">
                                <label>Địa Chỉ</label>
                                <input class="form-control" name="dia_chi" placeholder="Nhập địa chỉ" />
                            </div>
                             <div class="form-group">
                                <label>Ngày Sinh</label>
                                <input type = "date"class="form-control" name="ngay_sinh" placeholder="Nhập ngày sinh" />
                            </div>
                             <div class="form-group">
                                <label>SĐT</label>
                                <input type="number" class="form-control" name="sdt" placeholder="Nhập số điện thoại" />
                            </div>
                           
                            <div class="form-group">
                                <label>Giới Tính</label>
                                <label class="radio-inline">
                                    <input name="gioi_tinh" value="0" checked="" type="radio">Nam
                                </label>
                                <label class="radio-inline">
                                    <input name="gioi_tinh" value="1" type="radio">Nữ
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Cấp Độ</label>
                                <label class="radio-inline">
                                    <input name="cap_do" value="0" checked="" type="radio">Admin
                                </label>
                                <label class="radio-inline">
                                    <input name="cap_do" value="1" type="radio">SuperAdmin
                                </label>
                            </div>
                            <button type="submit" class="btn btn-default">Thêm</button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\do_an_2\resources\views/admin/use/them_admin.blade.php ENDPATH**/ ?>